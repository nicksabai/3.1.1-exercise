/**
 * Client side code.
 */
(function() {
    var app = angular.module("songApp", []);

    app.controller("songCtrl", ["$http", songCtrl]);

    function songCtrl($http) {
        var self = this; // vm

        self.lyrics = {
            verse1: "",
            verse2: "",
            verse3: "",
            verse4: "",
        };

        self.displayUser = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateOfBirth: ""
        };

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.gender = result.data.dateOfBirth;
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();